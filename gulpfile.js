var gulp = require('gulp');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var sourcemaps = require('gulp-sourcemaps');

var JS_SOURCE = 'js';
var JS_DEST = 'js';
var JS_OUTPUT_FILE = 'main_dest.js';
var CSS_SOURCE = 'scss';
var CSS_DEST = 'css';
var SERVER_BASE_DIR = './';
var WATCH_FILE_EXTENSIONS = ['*.html', '*.scss'];

gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: SERVER_BASE_DIR
    }
  });
});

gulp.task('bs-reload', function() {
  browserSync.reload();
});

gulp.task('scripts', function() {
  return gulp.src(JS_SOURCE + '/**/*.js')
    .pipe(plumber({
      errorHandler: function(error) {
        console.log(error.message);
        generator.emit('end');
    }}))
    .pipe(gulp.dest(JS_DEST + '/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('styles', function() {
  gulp.src(CSS_SOURCE + '/**/*.scss')

    .pipe(plumber({
      errorHandler: function(error) {
        console.log(error.message);
        generator.emit('end');
    }}))
     .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer('last 2 versions'))
     .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(CSS_DEST + '/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('default', ['browser-sync'], function() {
  gulp.watch(JS_SOURCE + '/**/*.js', ['scripts']);
  gulp.watch(CSS_SOURCE + '/**/*.scss', ['styles']);
  gulp.watch(WATCH_FILE_EXTENSIONS, ['bs-reload']);
});
