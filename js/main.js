var APP = APP || {};


var colors = {
    red: {
        coral: "coral",
        chinese: "chinese"
    },
    blue: {
        azure: "azure",
        navy: "navy",
        ice: "ice"
    },
    pink: {
        pink: "pink-pink",
        nectar: "nectar",
        azalea: "azalea"
    },
    orange: {
        tangerine: "tangerine",
        chocolate: "chocolate"
    },
    yellow: {
        sunshine: "sunshine",
        butter: "butter",
        gold: "gold"
    },
    green: {
        leaf: "leaf",
        khaki: "khaki",
        opal: "opal",
        turquoise: "turquoise"
    },
    purple: {
        violet: "violet",
        orchid: "orchid"
    },
    black: {
        charcoal: "charcoal",
        sterling: "sterling",
        smoke: "smoke",
        silver: "silver"
    },
    metal: {
        bronze: "bronze",
        champagne: "champagne",
        sand: "sand"
    },
    white: {
        cream: "cream"
    }
};

var styles = {
    amado: "amado",
    castela: "castela",
    chelsea: "chelsea",
    luca: "luca",
    nikita: "nikita",
    oberlin: "oberlin",
    terra: "terra",
    urbino: "urbino"
};

var bespoke = {
    made_to_order: "made_to_order",
    monogram: "monogram"
};

var category = {
    fitted: "fitted",
    flat: "flat"
};

var material = {
    percale: "percale",
    egyptian: "egyptian",
    cotton: "cotton",
    polyester: "polyester"
};


var data = [{
    id: "01",
    name: "Kiyoko",
    style: styles.amado,
    colors: {
        red: [
            colors.red.coral,
            colors.red.chinese
        ],
        blue: [
            colors.blue.azure,
            colors.blue.navy
        ],
        // white: [
        //     colors.white.cream
        // ],
        // metal: [
        //     colors.metal.champagne
        // ]
    },
    bespoke: bespoke.monogram,
    category: category.flat,
    material: material.egyptian,
    price_min: 100,
    price_max: 200
},
    {
        id: "02",
        name: "Alegro",
        style: styles.nikita,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "03",
        name: "GATSBY",
        style: styles.chelsea,
        colors: {
            blue: [
                colors.blue.navy
            ],
            metal: [
                colors.metal.champagne
            ],
            green: [
                colors.green.leaf
            ],
            yellow: [
                colors.yellow.sunshine
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "04",
        name: "Hamilton",
        style: styles.chelsea,
        colors: {
            red: [
                colors.red.chinese
            ],
            metal: [
                colors.metal.sand,
            ],
            white: [
                colors.white.cream,
            ],
            black: [
                colors.black.smoke
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "05",
        name: "KEY LARGO",
        style: styles.luca,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ],
            pink: [
                colors.pink.azalea,
                colors.pink.nectar
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "06",
        name: "Liana",
        style: styles.terra,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "07",
        name: "Luca",
        style: styles.urbino,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "08",
        name: "Marlowe",
        style: styles.oberlin,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "09",
        name: "Mirasol",
        style: styles.chelsea,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "10",
        name: "Nikita",
        style: styles.castela,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "11",
        name: "Nocturne",
        style: styles.castela,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "12",
        name: "Oberlin",
        style: styles.chelsea,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "13",
        name: "Poppy",
        style: styles.chelsea,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "14",
        name: "Portofino",
        style: styles.chelsea,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    },
    {
        id: "15",
        name: "SIERRA HEMSTITCH",
        style: styles.oberlin,
        colors: {
            red: [
                colors.red.coral
            ],
            blue: [
                colors.blue.azure,
                colors.blue.navy,
                colors.blue.ice
            ],
            metal: [
                colors.metal.champagne,
                colors.metal.bronze
            ],
            green: [
                colors.green.opal,
                colors.green.leaf
            ],
            purple: [
                colors.purple.orchid
            ],
            yellow: [
                colors.yellow.gold,
                colors.yellow.gold
            ]
        },
        bespoke: bespoke.made_to_order,
        category: category.fitted,
        material: material.polyester,
        price_min: 10,
        price_max: 300
    }
];

var selectedFilters = [];
var _template,
    _source,
    activeFilters = $('.mobile .selected'),
    prettyNames = [],
    filterClassList = "";


APP.main = (function () {
    function init() {


        $('.filters .body').toggle();

        populate(window.data, selectedFilters, colors, initSlider, hide4);
        mediaQ();
        mobileDrawers();
        eventHandlers();
        radials();




    }


    function colorGroups(items) {
        return items.map(function (t) {
            t.allcolors = (function () {
                var markup = "";
                var elems = t.colors;
                // if (t.colors.label === label) {
                //     return t;
                // }


                for (var k in elems) {

                    if (elems.hasOwnProperty(k)) {

                        elems[k].forEach(function (el, i) {

                            markup += '<a href="#" data-group-color="' + k + '" data-color="' + el + '" class="color-ball ' + el + '"></a>';

                        });
                        // markup += '" class="';
                        // elems[k].forEach(function(el,i) {
                        //     var space = i > 0 ? " " : "";
                        //     markup += space + el
                        // });
                        // markup += '"></a>'

                        // + data[k] +'" class="color-ball ' + data[k] + '"></a>';
                    }
                }
                markup = new Handlebars.SafeString(markup);
                return markup;

            }());
            t.flattencolors = (function () {
                var colors = [];
                for (var b in t.colors) {
                    colors = colors.concat(t.colors[b]);
                }
                return colors;
            }());
            t.extraimages = (function () {
                var images = [];
                for (var i = 0; i < t.flattencolors.length; i++) {
                    if (i > 5) {
                        images[i] = String(parseInt(i + 4));
                    } else {
                        images[i] = "0" + (i + 4);
                    }
                }
                return images;
            }());

            return t;
        });

    }


    function populate(data, filters, colors, slideFn, hideFn) {
        var list = $('.listing');
        var source = $("#list-template").html();
        var template = Handlebars.compile(source);
        list.append(template(colorGroups(data)));

        _source = $('#active-filters').html();
        _template = Handlebars.compile(_source);

        var colorSelector = $('.colors > div'),
            colorSrc = $('#color-template').html(),
            colorTemplate = Handlebars.compile(colorSrc);
        colorSelector.append(colorTemplate(colors));

        if (slideFn) {
            slideFn();
            hideFn();
        }
        // $('.listing li').addClass('inView'));

    }

    function hide4() {
        // elem.slice(4).hide();
        var elems = $('.listing li'),
            totals,
            eachlist;

        elems.each(function () {
            eachlist = $(this).find($('.color-ball'));
            totals = eachlist.length;
            eachlist.slice(4).hide();
            $(this).find('.remainder').append('+ ' + totals);
        });


    }

    function initSlider() {
        var _this,
            _caption,
            _counter,
            _elems = $('.listing li');

        _elems.each(function () {
            _this = $(this).find('figure');
            _caption = $(this).find('.slider-nav');

            _counter++;

            _this.slick({
                arrows: false,
                infinite: false,
                asNavFor: _caption,
                fade: true,
                cssEase: 'cubic-bezier(0.895, 0.03, 0.685, 0.22)',
                draggable: false,
                speed: 500
            });

            _caption.slick({
                asNavFor: _this,
                dots: false,
                focusOnSelect: true,
                arrows: false,
                infinite: false,
                slidesToShow: 15,
                variableWidth: true,
                centerMode: false,
                draggable: false
            });
        });
    }

    function mediaQ() {
        enquire.register("screen and (min-width:992px)", {

            // OPTIONAL
            // If supplied, triggered when a media query matches.
            match: function () {
                $('body').addClass('desktop');
            },
            unmatch: function () {
                $('body').removeClass('desktop');
            }
        });

        enquire.register("screen and (max-width: 991px)", {
            match: function () {
                $('body').addClass('mobile');
            },
            unmatch: function () {
                $('body').removeClass('mobile');
            }
        });
    }

    function updateActive(_target) {

        var attributes = _target.data('filter');
        var prettyAttr = _target.html();
        // console.log('attr: ', attributes);


        var selectedElems = $('.listing li' + '.' + attributes);
        var _tempComparison = attributes.split(" ");
        var crossedFilters = [];

        // console.log(selectedElems, '.listing li.inView' + '.' + attributes);
        $(".colors a, .sheets a, .material a, .bespoke a").removeClass('disabled');
        selectedFilters = [];
        console.log(selectedFilters)

        selectedElems.each(function (e, i) {
            var _tempFilter = $(this).data('filters').split(" ");
            var colors = {};
            addToFilters(attributes, prettyAttr, _target);
            $('.listing li.' + attributes).each(function(i, el) {
                $(this).data('filters').split(' ').slice(1).forEach(function(el) {
                    colors[el] = true;
                })
            });
            console.log(colors);
            $(".colors a, .material a, .sheets a, .bespoke a").not(
                Object.keys(colors).map(function(el) {
                    return '.' + el
                }).join(', '))
                .addClass('disabled');
            // $(".style a").not().addClass('disabled');
            // document.querySelectorAll('.listing li.style-amado')[0].getAttribute('data-filters').split(' ').slice(1)
            // var selector = '.categories .' + attributes;
            // var noFilters = [];

            // if (attributes.includes("style-")) {
            //     // console.log(attributes, prettyAttr, _target);
            //     addToFilters(attributes, prettyAttr, _target);
            // } else {
            //     if (!_tempFilter.includes(attributes)) {

            //         // console.log("no attribute " + attributes);
            //         $(selector).addClass('disabled');
            //         noFilters.push(_tempFilter);

            //     } else {
            //         // console.log('tem atributo' + attributes);
            //         $(selector).removeClass('disabled');
            //         addToFilters(attributes, prettyAttr, _target);
            //     }
            //     var index = _tempFilter.indexOf(attributes);    // <-- Not supported in <IE9
            //     if (index !== -1) {
            //         _tempFilter.splice(index, 1);
            //     }
            //     _tempFilter.join(", .");
            //     // console.log(_tempFilter);
            // }
            // // console.log(selector, selectedFilters, _tempFilter);

        });




        _source = $('#active-filters').html();
        _template = Handlebars.compile(_source);
        activeFilters.html(_template(selectedFilters));


        if (selectedFilters.length > 0) {
            $('.listing li').delay(200).hide().removeClass('inView');
            filterClassList = selectedFilters.join(", .");
        } else {
            $('.listing li').show().addClass('inView');
        }
        _target.toggleClass('enabled');


        var _selector = "." + filterClassList;
        _selector = _selector[_selector.length - 1] === '.' ?
            _selector.split('.').filter(function(el) { !!el }).join('.')
            : '';
        $('.listing li' + _selector).show().addClass('inView');

        $('.close').on('click', function () {
            var attr = $("[data-filter='" + $(this).data('filter') + "'");
            updateActive($(this));
            attr.removeClass('enabled');
        });

        $('window').on('scroll', stickIt);


    }

    function radials() {
        $('.circle-mask').each(function (el, i) {
            var inside = $(this).find('.colors-wheel');
            var semicircles = el.children.length;
            if (semicircles > 1) {
                var degreesI = 360 / semicircles;
                // translate(15px,0) rotate(135deg);
                inside.each(function(e, i) {
                   $(this).css('transform', ' translate(15px,0) rotate(' +(degressI * i) + 'deg)')
                })
            }
        })
    }

    function eventHandlers() {
        var body = $('body, html');

        $('.categories a, .colors a, button, .close, .slider-nav a, a[href="#"]').on('click', function (e) {
            e.preventDefault();
        });
        $('.categories a, a.close').on('click', function (e) {
            updateActive($(this));
        });


        $('.refine').on('click', function () {
            var elScroll = $('.filters header').offset().top;
            var windowScroll = body.offset().top;
            var scrollVal = elScroll - windowScroll;
            $('.filters .body').stop().slideToggle(0).toggleClass('active');
            $('.filters').toggleClass('active');
            if (!$(this).hasClass('active') && ($('.filters:not(.fixed)'))) {
                $('body, html').stop().animate({scrollTop: scrollVal}, 900, "swing");
            }
            $(this).toggleClass('active');

        });

        $(window).on('scroll', throttle(stickIt, 10));

    }

    var fixedTop = $('.filters').offset().top;

    function stickIt() {

        if ($(window).scrollTop() >= fixedTop) {
            $('.filters').addClass('fixed');
        } else {
            $('.filters').removeClass('fixed');
        }

    }

    function throttle(fn, wait) {
        var time = Date.now();
        return function () {
            if ((time + wait - Date.now()) < 0) {
                fn();
                time = Date.now();
            }
        }
    }

    function addToFilters(filters, pretty) {

        if (selectedFilters) {
            var _tempFilter = selectedFilters.indexOf(filters);
            var _tempKey = selectedFilters[_tempFilter];
            if (selectedFilters.includes(filters)) {
                // console.log(_tempFilter);
                selectedFilters.splice(_tempFilter, 1);
                prettyNames.splice(_tempFilter, 1);
                // console.log("exists");
            }
            else {
                selectedFilters.push(filters);
                prettyNames.push(pretty);
                // console.log('');
                // console.log("didn't exist");

            }
            // console.log(_tempKey, selectedFilters);

        } else {
            selectedFilters.push(filters);
            prettyNames.push(pretty);
        }

    }

    function mobileDrawers() {
        if ($('body.mobile')) {
            $('.categories h3').on('click', function () {
                $(this).parent().find('div, ul').toggleClass('open');
            })
        } else {
            return
        }

        $('.mobile-apply').on('click', function () {
            $('.filters .body').toggle();
        });
        $('.mobile-reset, .reset').on('click', function () {
            selectedFilters = [];
            filterClassList = "";
            _template = Handlebars.compile(_source);
            activeFilters.html(_template(selectedFilters));


            $(".categories a.enabled").removeClass('enabled');
            // $('.filters .body').toggle();
            $('.listing li').show().addClass('inView');
        });
    }


    return {
        init: init
    };
})();


$(document).ready(function () {
    APP.main.init();
});